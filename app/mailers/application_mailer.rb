class ApplicationMailer < ActionMailer::Base
  default from: 'sharee.ongraph@gmail.com'
  layout 'mailer'
end
